public with sharing class ASPR_PeopleController {
    
    class Person {
        @AuraEnabled
        public String name {get; set;}
        @AuraEnabled
        public Date birthDate {get; set;}
        @AuraEnabled
        public Integer id {get; set;}
    }

    static Map<Integer, Person> peopleData = new Map<Integer, Person>();

    static {
        List<Person> peopleList = (List<Person>)JSON.deserialize(
            '[{"id": 1, "name": "Francois","birthDate": "1980-04-05"},{"id": 2, "name": "Jean Claude","birthDate": "1995-02-18"},{"id": 3, "name": "Marie","birthDate": "1991-10-23"},{"id": 4, "name": "Cecile","birthDate": "1991-01-04"},{"id": 5, "name": "Barbara","birthDate": "1998-08-30"}]',
            List<Person>.class
        );

        for (Person person : peopleList) {
            peopleData.put(person.id, person);
        }
    }

    @AuraEnabled(cacheable=true)
    public static Map<Integer, Person> getPeopleBornAfter(String minDate) {
        Date minDateParsed = Date.valueOf(minDate);
        Map<Integer, Person> result = new Map<Integer, Person>();
        for (Integer id : peopleData.keySet()) {
            Person person = peopleData.get(id);
            if (person.birthDate >= minDateParsed) {
                result.put(id, person);
            }
        }

        return result;
    }
}


