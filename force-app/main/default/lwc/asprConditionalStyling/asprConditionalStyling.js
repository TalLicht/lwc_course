import { LightningElement, track } from 'lwc';

export default class AsprConditionalStyling extends LightningElement {
    @track shadowColor = "";
    @track backgroundColor = "";
    @track radius = 25;
    @track isAnimated = false;

    onSetValue(e) {
        const { property } = e.target.dataset;
        this[property] = e.target.value;
    }

    toggleAnimation() {
        this.isAnimated = !this.isAnimated;
    }

    get style() {
        return `
            background-color: ${this.backgroundColor};
            width: ${this.radius * 2}px;
            height: ${this.radius * 2}px;
            box-shadow: inset 0 0 10px ${this.shadowColor};
        `
    }

    get className() {
        return `shape ${this.isAnimated ? "animated" : "circle"}`;
    }
}