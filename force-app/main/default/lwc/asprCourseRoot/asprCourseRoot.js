import { LightningElement, track } from 'lwc';
import conditionalStyling from './templates/conditionalStyling.html';
import codeSharing from './templates/codeSharing.html';
import externalLibraries from './templates/externalLibraries.html';
import sideEffects from './templates/sideEffects.html';
import apex from './templates/apex.html';
import customIcons from './templates/customIcons.html';

const templates = [
    conditionalStyling,
    codeSharing,
    externalLibraries,
    sideEffects,
    apex,
    customIcons
];

export default class AsprCourseRoot extends LightningElement {
    @track selectedIndex = 0;
    templatesLength = templates.length;

    get selectedTemplate() {
        return templates[this.selectedIndex];
    }

    get isBackDisabled() {
        return this.selectedIndex === 0;
    }

    get isForwardDisabled() {
        return this.selectedIndex === this.templatesLength - 1;
    }

    get backClassName() {
        return this.isBackDisabled ? "disabled" : "clickable";
    }

    get forwardClassName() {
        return this.isForwardDisabled ? "disabled" : "clickable";
    }

    onBack() {
        if (!this.isBackDisabled) {
            this.selectedIndex -= 1;
        }
    }

    onForward() {
        if (!this.isForwardDisabled) {
            this.selectedIndex += 1;
        }
    }

    jumpToTemplate(i) {
        this.selectedIndex = i;
    }

    onClickBullet({ detail }) {
        this.jumpToTemplate(detail);
    }

    onKeyDown(e) {
        if (!e || document.activeElement === this.template.host) {
            return;
        }

        switch (e.keyCode) {
            case 37:
                this.onBack();
                return;
            case 39:
                this.onForward();
                return;
        }
    }

    connectedCallback() {
        this.boundOnKeyDown = this.onKeyDown.bind(this);
        document.addEventListener("keydown",  this.boundOnKeyDown, true);
    }

    disconnectedCallback() {
        document.removeEventListener("keydown", this.boundOnKeyDown, true);
    }
    
    errorCallback(error) {
        console.error(new Error(error));
    }
}