import { LightningElement, track } from 'lwc';
import { enhance, enhancers, mockDataHandler } from 'c/asprUtils';
import momentScript from '@salesforce/resourceUrl/moment';

const { withOnSetValue, withDependencies } = enhancers;


class AsprSideEffects extends LightningElement {
    peopleData = {};
    dateFormat = "ddd, MMM DD YYYY";
    
    @track isLoadingData = false;

    _minDate = null;

    get minDate() {
        return this._minDate;
    }

    set minDate(date) {
        this._minDate = date;
        this.isLoadingData = true;
        mockDataHandler.getPeopleBornAfter(date).then(people => {
            this.peopleData = people;
            this.isLoadingData = false;
        })
    }

    get isLoadingAll() {
        // Loading from component + from withDependencies
        return this.isLoadingData || this.isLoading;
    }

    get peopleToShow() {
        // This property is from the withDependencies enhancer
        if (this.isLoadingAll) {
            return [];
        }

        const { moment } = window;
        return Object.values(this.peopleData)
            .map(person => ({
                ...person,
                birthDate: moment(person.birthDate).format(this.dateFormat)
            }))
    }

    get isDataEmpty() {
        return Object.keys(this.peopleData).length === 0;
    }

    get peopleTitle() {
        const numberOfPeopleFound = Object.keys(this.peopleData).length;
        return `${numberOfPeopleFound || "No"} people born after ${moment(this.minDate).format(this.dateFormat)}`;
    }
}

export default enhance(AsprSideEffects, [ 
    withOnSetValue(),
    withDependencies([ momentScript ])
]);