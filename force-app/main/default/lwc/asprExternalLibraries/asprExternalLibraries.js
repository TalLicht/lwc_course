import { LightningElement, track } from 'lwc';
import { objectUtils, peopleData, enhance, enhancers } from 'c/asprUtils';
import momentScript from '@salesforce/resourceUrl/moment';

const { withOnSetValue, withDependencies } = enhancers;

class AsprExternalLibraries extends LightningElement {
    data = peopleData;
    
    @track search;
    @track selectedPeople = [];

    get options() {
        return Object.keys(this.data).map(personId => ({ 
            value: personId, 
            label: this.data[personId].name 
        }))
    }

    get peopleToShow() {
        // This property is from the withDependencies enhancer
        if (this.isLoading) {
            return [];
        }

        const { moment } = window;
        const filteredPeople = objectUtils.omit(this.data, this.selectedPeople);
        return Object.values(filteredPeople)
            .filter(person => {
                if (!this.search) {
                    return true;
                }

                return person.name.includes(this.search);
            })
            .map(person => ({
                ...person,
                birthDate: moment(person.birthDate).format("ddd, MMM DD")
            }))
    }

    onPeopleListboxChange(e) {
        this.selectedPeople = e.target.value;
    }

    connectedCallback() {
        console.log("MOMENT INSTANCE: ", window.moment);
    }

}

export default enhance(AsprExternalLibraries, [ 
    withOnSetValue(),
    withDependencies([ momentScript ])
]);