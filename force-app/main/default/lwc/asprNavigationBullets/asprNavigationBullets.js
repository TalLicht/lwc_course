import { LightningElement, api } from 'lwc';
import { constants } from 'c/asprUtils';
const { customEvents } = constants;

export default class AsprNavigationBullets extends LightningElement {
    @api numberOfBullets = 0;
    @api selectedBullet = null;

    get bulletsArray() {
        const result = [];
        for (let i=0; i < this.numberOfBullets; i++) {
            const customEvent = new CustomEvent(customEvents.clickbullet, { detail: i });
            const isSelected = (this.selectedBullet || this.selectedBullet === 0) && i === this.selectedBullet;
            result.push({
                index: i,
                onClick: () => {
                    this.dispatchEvent(customEvent);
                },
                class: `bullet ${isSelected ? "selected" : ""}`
            })
        }

        return result;
    }
}