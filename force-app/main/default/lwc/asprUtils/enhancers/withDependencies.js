import { loadScriptWithCache } from "../utils/loadScriptWithCache";

export const withDependencies = (dependencies = []) => {
    return WrappedComponent => {
        const originalConnectedCallback =  WrappedComponent.prototype.connectedCallback;
        return class extends WrappedComponent {
            isLoading = true;

            connectedCallback() {
                Promise.all(dependencies.map(script => loadScriptWithCache(this, script))).then(() => {
                    typeof originalConnectedCallback === "function" && originalConnectedCallback.call(this);
                    this.isLoading = false;
                })
            }
        }
    }
}