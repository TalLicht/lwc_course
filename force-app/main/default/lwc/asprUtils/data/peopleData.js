export const peopleData = {
    1: {
        id: 1,
        name: "Francois",
        birthDate: "1980-04-05"
    },
    2: {
        id: 2,
        name: "Jean Claude",
        birthDate: "1995-02-18"
    },
    3: {
        id: 3,
        name: "Marie",
        birthDate: "1991-10-23"
    },
    4: {
        id: 4,
        name: "Cecile",
        birthDate: "1991-01-04"
    },
    5: {
        id: 5,
        name: "Barbara",
        birthDate: "1998-08-30"
    },
}