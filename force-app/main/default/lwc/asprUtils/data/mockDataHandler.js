import { peopleData } from "./peopleData";
import { filter } from "../utils/objectUtils";
import { delay } from "../utils/delay";

export const getPeopleBornAfter = date => {
    return delay(500).then(() => filter(peopleData, person => new Date(person.birthDate) >= new Date(date)));
}