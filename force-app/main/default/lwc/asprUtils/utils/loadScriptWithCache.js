import { loadScript } from 'lightning/platformResourceLoader';

export const loadScriptWithCache = (component, src) => {
    const scriptElement = document.querySelector('script[data-locker-src=' + `"${src}"` + "]");
    return scriptElement ? Promise.resolve() : loadScript(component, src);
}