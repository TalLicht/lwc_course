export const delay = (ms) => {
    return new Promise((resolve, reject) => {
        try {
            setTimeout(resolve, ms);
        } catch(err) {
            reject(err);
        }
    })
}