import { LightningElement, track, wire } from 'lwc';
import { enhance, enhancers } from 'c/asprUtils';
import momentScript from '@salesforce/resourceUrl/moment';
import getPeopleBornAfter from '@salesforce/apex/ASPR_PeopleController.getPeopleBornAfter';

const { withOnSetValue, withDependencies } = enhancers;


class AsprApex extends LightningElement {
    peopleData = {};
    dateFormat = "ddd, MMM DD YYYY";
    
    @track isLoadingData = false;

    // This should be undefined to not be called by wire
    _minDate;

    get minDate() {
        return this._minDate;
    }

    /* Imperative Apex */
    // set minDate(date) {
    //     this._minDate = date;
    //     this.isLoadingData = true;
    //     // imperative apex
    //     getPeopleBornAfter({ minDate: date }).then(
    //         people => {
    //             this.peopleData = people;
    //             this.isLoadingData = false;
    //         }, 
    //         e => console.error(e)
    //     );
    // }

    /* Wire With Property */
    // set minDate(date) {
    //     this._minDate = date;
    // }

    // @wire(getPeopleBornAfter, { minDate: "$minDate" }) wiredPeople;
    // get peopleData() {
    //     // You can't put loading logic here! Can depend on other stuff
    //     return this.wiredPeople.data || {};
    // }

    /* Wire With Function */
    set minDate(date) {
        this._minDate = date;
        this.isLoadingData = true;
    }

    @wire(getPeopleBornAfter, { minDate: "$minDate" })
    wiredPeople({ data, error }) {
        if (error) {
            console.error(error);
            return;
        }

        if (data) {
            this.peopleData = data;
            this.isLoadingData = false;
        }
    }
    

    get isLoadingAll() {
        // Loading from component + from withDependencies
        return this.isLoadingData || this.isLoading;
    }

    get peopleToShow() {
        // This property is from the withDependencies enhancer
        if (this.isLoadingAll) {
            return [];
        }

        const { moment } = window;
        return Object.values(this.peopleData)
            .map(person => ({
                ...person,
                birthDate: moment(person.birthDate).format(this.dateFormat)
            }))
    }

    get isDataEmpty() {
        return Object.keys(this.peopleData).length === 0;
    }

    get peopleTitle() {
        const numberOfPeopleFound = Object.keys(this.peopleData).length;
        return `${numberOfPeopleFound || "No"} people born after ${moment(this.minDate).format(this.dateFormat)}`;
    }
}

export default enhance(AsprApex, [ 
    withOnSetValue(),
    withDependencies([ momentScript ])
]);