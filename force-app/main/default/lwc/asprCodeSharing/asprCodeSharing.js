import { LightningElement, track } from 'lwc';
import { objectUtils, peopleData, enhance, enhancers } from 'c/asprUtils';
const { withOnSetValue } = enhancers;

class AsprCodeSharing extends LightningElement {
    data = peopleData;
    
    @track search;
    @track selectedPeople = [];

    get options() {
        return Object.keys(this.data).map(personId => ({ 
            value: personId, 
            label: this.data[personId].name 
        }))
    }

    get peopleToShow() {
        const filteredPeople = objectUtils.omit(this.data, this.selectedPeople);
        return Object.values(filteredPeople).filter(person => {
            if (!this.search) {
                return true;
            }

            return person.name.includes(this.search);
        });
    }

    onPeopleListboxChange(e) {
        this.selectedPeople = e.target.value;
    }

}

export default enhance(AsprCodeSharing, [ withOnSetValue() ]);