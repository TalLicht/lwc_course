import { LightningElement, api } from 'lwc';
import svg1 from "./icons/svg1.html";
import svg2 from "./icons/svg2.html";
import empty from "./icons/empty.html";
 
 
const iconMap = {
    svg1,
    svg2
}
 
export default class Child extends LightningElement {
    @api color = "black";
    @api iconName;
 
    render() {
        return iconMap[this.iconName] || empty;
    }
}